<?php
class PostsController extends AppController
{
	 var $helpers = array ('Html','Form');
	 public $components = array('Session');
	 var $name= 'Posts';
	 function index() {
	  /* $posts=$this->Post->find('all');
	  // $this->set(compact('posts'));
	   //$this->set('posts', $this->Post->find('all'));
	   $channelData = array('title' =>'Current Post | The blogger' ,
	   									'link' => array(
	   										'controller' => 'posts', 'action' =>'index','ext' =>'rss'),
	   									'url' =>  array('controller' => 'posts', 'action' =>'index','ext' =>'rss'),
	   									'description' => 'The current posts in our blog',
	   									'language' => 'en-uk' );
	   $posts = $this->Post->find('all',
	   							array('limit' =>10, 'order' => 'Post.created'));
	   $this->set(compact('channelData','posts'));*/
	   $post=$this->Post->find('all', array('order'=>'id desc'));
        $this->set('post',$post);
	}

	function add() {
	  $actionHeading = 'Add a Post!';
	  $actionSlogan = 'Please fill in all fields. Feel free to add your poast and express your opinion.';

	  $this->set(compact('actionHeading','actionSlogan'));

	  if(!empty($this->data)){
	   $this->Post->create();
	   if($this->Post->save($this->data)){
		$this->Session->setFlash(__('The Post has been saved',true));
		$this->redirect(array('action'=>'index'));
	   }
	   else {
		$this->Session->setFlash(__('the Post could not be saved. Please try again.',true));
	   }
	  }
	}
	public function view($id) {
        $post = $this->Post->findById($id);
        $this->set(array(
            'post' => $post,
            '_serialize' => array('post')
        ));
    }
	/*
	public function edit($id = null) {
        $actionHeading = 'Edit a Post';
		$actionSlogan = 'Please fill in all fields. Now you can amend your post.';
		
		$this->set(compact('actionHeading','actionSlogan'));
		
		if(!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Post', true));
			$this->redirect(array('action'=>'index'));
		}
		
		if(empty($this->data)) {
			if($this->Post->save($this->data)) {
				$this->Session->setFlash(__('The Post has been saved', true));
			$this->redirect(array('action'=>'index'));
			}else{
				$this->Session->setFlash(__('The Post could not be saved.Please try again', true));
			}
		}
		
		if(empty($this->data)) {
			$this->data = $this->Post->read(null, $id);
			}
		
    }*/
	function edit($id = null) {
    $this->Post->id = $id;
    if (empty($this->data)) {
        $this->data = $this->Post->read();
    } else {
        if ($this->Post->save($this->data)) {
            $this->Session->setFlash('Your post has been updated.');
            $this->redirect(array('action' => 'index'));
        }
    }
}

    public function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Post',true));
			$this->redirect(array('action'=>'index'));
        }
		if ($this->Post->delete($id)) {
			$this->Session->setFlash(__(' Post Deleted',true));
			$this->redirect(array('action'=>'index'));
        }
        
	}
	
	public function disable($id = null) {
		$post = $this->Post->read(null,$id);
        if (!$id && empty($post)) {
            $this->Session->setFlash(__('You must provide a valid ID number to disable a Post',true));
			$this->redirect(array('action'=>'index'));
        }
		if (!empty($post)) {
			$post['Post']['published'] = 0;
			if ($this->Post->save($post))
			{
				$this->Session->setFlash(__('Post ID '.$id.' has been disabled.', true));
			}
			else {
				$this->Session->setFlash(__('Post ID '.$id.' was not saved.', true));
			}
			$this->redirect(array('action'=>'index'));
			
        }else {
			$this->Session->setFlash(__('No Post by that ID was found', true));
			$this->redirect(array('action'=>'index'));
		}
        
	}

	public function enable($id = null) {
        $post = $this->Post->read(null,$id);

        if (!$id && empty($post)) {
            $this->Session->setFlash(__('You must provide a valid ID number to enable a Post',true));
			$this->redirect(array('action'=>'index'));
        }
        if (!empty($post)) {
			$post['Post']['published'] = 1;
			if ($this->Post->save($post))
			{
				$this->Session->setFlash(__('Post ID '.$id.' has been published.', true));
			}
			else {
				$this->Session->setFlash(__('Post ID '.$id.' was not saved.', true));
			}
			$this->redirect(array('action'=>'index'));
			
        }else {
			$this->Session->setFlash(__('No Post by that ID was found', true));
			$this->redirect(array('action'=>'index'));
		}
    }
	
	
	
 
 
}
?>