<?php echo $this->form->create('Task');?> 
             <fieldset> 
                 <legend>Edit Task</legend> 
                 <?php 
                     echo $this->form->hidden('id'); 
                     echo $this->form->input('title'); 
                     echo $this->form->input('done'); 
                 ?> 
             </fieldset> 
         <?php echo $this->form->end('Save');?>
  <?php echo $this->html->link('List All Tasks', array('action'=>'index')); ?><br /> 
     <?php echo $this->html->link('Add Task', array('action'=>'add')); ?> 
     <?php echo $this->html->link('List Done Tasks', array('action'=> 
                                                         'index', 'done')); ?><br /> 
          <?php echo $this->html->link('List Pending Tasks', array('action'=> 
                                                         'index', 'pending')); ?><br /> 