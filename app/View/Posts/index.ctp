<!-- File: /app/views/posts/index.ctp -->

<h1>Blog posts</h1>
<h1><?php echo $this->Html->link("Home","");?><?php echo(" "); echo $this->Html->link("Add New post","add");?></h1>

<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Content</th>
        <th>Last Modified</th>
        <th>Created</th>
        <th  colspan="3">&nbsp&nbsp&nbsp&nbsp Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php echo $this->Html->link($post['Post']['title'],
array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
        </td>
		<td>
            <?php echo $this->Html->link($post['Post']['content'],
array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
        </td>
		<td><?php echo $post['Post']['modified']; ?></td>
        <td><?php echo $post['Post']['created']; ?></td>
	
	<td>
		 <?php 
			$temp = $post['Post']['published'];
			if($temp==1){
				echo $this->Html->link("Publish",
				array('controller' => 'posts', 'action' => 'view', $post['Post']['id']));
			}
			else{
				echo $this->Html->link($post['Post']['published'],
					array('controller' => 'posts', 'action' => 'view', $post['Post']['id']));
			} ?>
	</td>
		<td> <?php
       echo $this->Html->link(
            'Edit',
            '/posts/edit/'.$post['Post']['id']);?>
		</td>
		<td><?php 
       echo  $this->Html->link(
            'Delete',
            '/posts/delete/'.$post['Post']['id']);?>
			</td>
      
     
    
    </tr>
    <?php endforeach; ?>

</table>