<fieldset>
 <legend><?php __('Add a Post!');?></legend>
 Please fill in all fields.
 <?php
  echo $this->form->create('Post');
  echo $this->form->error('Post.title');
  echo $this->form->input('Post.title',
       array('id'=>'posttitle','label'=>'Title:',
        'size'=>'50','maxlength'=>'255','error'=>false));
  echo $this->form->end(array('label'=>'Submit Post'));
  ?>
</fieldset>

